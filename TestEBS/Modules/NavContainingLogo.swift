//
//  NavContainingLogo.swift
//  TestEBS
//
//  Created by Dinu_c on 2/27/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import UIKit

protocol NavContainingLogo: class {
    func addLogo()
}

extension NavContainingLogo where Self: UIViewController {
    func addLogo() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "Header.png")
        self.navigationItem.titleView = imageView
    }
}
