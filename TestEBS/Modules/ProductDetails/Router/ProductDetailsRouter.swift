//
//  ProductDetailsRouter.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 26/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductDetailsRouter {

}

class ProductDetailsRouterImp: ProductDetailsRouter {

    weak var viewController: ProductDetailsViewController!

}
