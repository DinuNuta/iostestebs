//
//  ProductDetailsPresenter.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 26/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductDetailsPresenter {
    func refresh()
    func viewDidLoad()
    func tappedOnFavorit()
}

class ProductDetailsPresenterImp: ProductDetailsPresenter {

    weak var view: ProductDetailsView!
    var interactor: ProductDetailsInteractor!
    var router: ProductDetailsRouter!
    var productId: Int?
    var product: Product? {
        didSet {
            guard let prod = product else {return}
            self.view.configView(with: prod)
        }
    }

    init(view: ProductDetailsView, interactor: ProductDetailsInteractor, router: ProductDetailsRouter,
         productId: Int? = nil, product: Product? = nil) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.productId = productId
        guard let prod = product else { return }
        self.product = prod
        self.productId = prod.id
    }
}

extension ProductDetailsPresenterImp {
    func refresh() {
        guard let id = productId else {return}
        interactor.getProductInfo(by: id) {[weak self] (result) in
            guard let self = self else {return}
            do {
                let isFavorit = self.product?.isFavorite ?? false
                let prod = try result.get()
                prod.isFavorite = isFavorit
                self.product = prod
                if isFavorit {
                    try self.interactor.add(product: prod)
                }
            } catch {
                self.view.didEndRefrehing()
                self.view.showAlertError(error: error)
            }
        }
    }

    func viewDidLoad() {
        guard let prod = product else {
            self.refresh()
            return
        }

        self.view.configView(with: prod)
    }

    func tappedOnFavorit() {
        guard let prod = product else {return}
        do {
            try interactor.liked(product: prod)//and updated product fields
        } catch {
            self.view.showAlertError(error: error)
        }
    }
}
