//
//  ProductDetailsViewController.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 26/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit

protocol ProductDetailsView: class, AlertErrorPresenting {
    func didBeginRefrehing()
    func didEndRefrehing()
    func configView(with product: Product)

}

class ProductDetailsViewController: UIViewController, ProductDetailsView, NavContainingLogo {

    var presenter: ProductDetailsPresenter!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shortDetailLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var butomMenu: UIView!
    @IBOutlet weak var favoritButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        butomMenu.setShadow()
        addLogo()
        configureRefreshControl()
        presenter.viewDidLoad()
    }

    func configureRefreshControl () {
        // Add the refresh control to your UIScrollView object.
        scrollView.refreshControl = UIRefreshControl()
        scrollView.refreshControl?.addTarget(self, action: #selector(handleRefreshControl),
                                             for: .valueChanged)
    }

    @objc func handleRefreshControl() {
        presenter.refresh()
    }

    @IBAction func tapOnFavorit(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        presenter.tappedOnFavorit()
    }
}

extension ProductDetailsViewController: PriceAtributedString {
    func didEndRefrehing() {
        DispatchQueue.main.async {
            self.scrollView.refreshControl?.endRefreshing()
        }
    }

    func didBeginRefrehing() {
        DispatchQueue.main.async {
            self.scrollView.refreshControl?.beginRefreshing()
        }
    }

    func configView(with product: Product) {
        didEndRefrehing()
        if let img = product.image,
            let imgUrl = URL(string: img) {
            self.productImageView.kf.setImage(with: imgUrl)
        } else {
            print(product.image)
            //TODO: set no img icon
        }
        let attributedPrice = priceAtributedString(price: product.price, salePrecent: product.salePrecent)
        DispatchQueue.main.async {
            self.favoritButton.isSelected = product.isFavorite
            self.titleLabel.text = product.title
            self.shortDetailLabel.text = product.shortDescription
            self.priceLabel.attributedText = attributedPrice
            self.detailsTextView.text = product.details
            self.contentView.isHidden = false
        }
    }
}
