//
//  DetailsProductServices.swift
//  TestEBS
//
//  Created by Dinu_c on 2/27/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire
protocol DetailsProductServices: BaseServices {
    func getProductInfo(by id: Int, completion: @escaping (Result<Product>) -> Void)
}

struct DetailsProductServicesImp: DetailsProductServices {
    var requestManager: SessionManager = Alam_DefaultHeaders.sender

    func getProductInfo(by id: Int, completion: @escaping (Result<Product>) -> Void) {
        let params: [String: Any] = [NK.id.rawValue: id]
        request(RequestHttp(url: URLs.product, method: .get, parameters: params), completion: completion)
    }
}
