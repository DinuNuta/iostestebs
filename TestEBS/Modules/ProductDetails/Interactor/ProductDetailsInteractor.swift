//
//  ProductDetailsInteractor.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 26/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductDetailsInteractor {
    func getProductInfo(by id: Int, completion: @escaping (Result<Product>) -> Void)
    func add(product: Product) throws
    func liked(product: Product) throws
}

class ProductDetailsInteractorImp: ProductDetailsInteractor {
    var services: DetailsProductServices!
    var realmManager: RealmManager {RealmManagerImp.shared}
    init(services: DetailsProductServices) {
        self.services = services
    }

    func getProductInfo(by id: Int, completion: @escaping (Result<Product>) -> Void) {
        services.getProductInfo(by: id, completion: completion)
    }

    func add(product: Product) throws {
        try realmManager.add(entity: product)
    }

    func liked(product: Product) throws {
        try realmManager.liked(product: product)
    }

}
