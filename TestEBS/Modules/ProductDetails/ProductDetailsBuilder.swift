//
//  ProductDetailsBuilder.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 26/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import Swinject

protocol ProductDetailsBuilder {
    func buildProductDetailsViewController(with productId: Int?, product: Product?) -> ProductDetailsViewController!
}

class ProductDetailsDefaultBuilder: ProductDetailsBuilder {

    let container = Container()

    func buildProductDetailsViewController(with productId: Int? = nil, product: Product? = nil) -> ProductDetailsViewController! {
        registerView()
        registerInteractor()
        registerRouter()
        registerPresenter(productId: productId, product: product)

        return container.resolve(ProductDetailsViewController.self)!
    }

    fileprivate func registerView() {
        container.register(ProductDetailsViewController.self) { _ in

            ProductDetailsViewController(nibName: String(describing: ProductDetailsViewController.self), bundle: Bundle.main)

        }.initCompleted { res, vc in

            vc.presenter = res.resolve(ProductDetailsPresenter.self)
        }
    }

    fileprivate func registerInteractor() {
        container.register(ProductDetailsInteractor.self) { _ in
            ProductDetailsInteractorImp(services: DetailsProductServicesImp())
        }
    }

    fileprivate func registerRouter() {
        container.register(ProductDetailsRouter.self) { res in
            let router = ProductDetailsRouterImp()
            router.viewController = res.resolve(ProductDetailsViewController.self)!
            return router
        }
    }

    fileprivate func registerPresenter(productId: Int?, product: Product?) {
        container.register(ProductDetailsPresenter.self) { res in
            ProductDetailsPresenterImp(view: res.resolve(ProductDetailsViewController.self)!,
                                       interactor: res.resolve(ProductDetailsInteractor.self)!,
                                       router: res.resolve(ProductDetailsRouter.self)!,
                                       productId: productId,
                                       product: product)
        }
    }

    deinit {
        container.removeAll()
    }
}
