//
//  DetailsProduct.swift
//  TestEBS
//
//  Created by Dinu_c on 2/27/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

struct DetailsProduct: Codable {
    let id: Int
    let title: String
    let shortDescription: String?
    let image: String?
    let price, salePrecent: Double
    let details: String?

    enum CodingKeys: String, CodingKey {
        case id, title
        case shortDescription
        case image, price
        case salePrecent
        case details
    }
}
