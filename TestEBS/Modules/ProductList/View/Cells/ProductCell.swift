//
//  ProductCell.swift
//  TestEBS
//
//  Created by Dinu_c on 2/26/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import Kingfisher

extension ProductCell: NibLoadableView {}

class ProductCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var shortDetailLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var favoritButton: UIButton!

    var favoriteClouser: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.productImageView.kf.cancelDownloadTask()
        self.productImageView.image = nil

        self.titleLabel.text = nil
        self.shortDetailLabel.text = nil
        self.priceLabel.attributedText = nil
        self.favoriteClouser = nil
    }

    func config(with product: Product) {
        if let img = product.image,
            let imgUrl = URL(string: img) {
            self.productImageView.kf.setImage(with: imgUrl, placeholder: UIImage(named: "img-placeholder"))
        } else {
            DispatchQueue.main.async {
                self.productImageView.image = UIImage(named: "img-placeholder")
            }
        }
        let attributedPrice = priceAtributedString(price: product.price, salePrecent: product.salePrecent)
        DispatchQueue.main.async {
            self.favoritButton.isSelected = product.isFavorite
            self.titleLabel.text = product.title
            self.shortDetailLabel.text = product.shortDescription
            self.priceLabel.attributedText = attributedPrice
        }
    }

    @IBAction func tapOnFavorite(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        favoriteClouser?()
    }
}

extension ProductCell: PriceAtributedString {}
