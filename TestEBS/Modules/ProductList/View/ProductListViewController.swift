//
//  ProductListViewController.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 25/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import ESPullToRefresh

protocol ProductListView: class, AlertErrorPresenting {
    func reloadTable()
    func didEndRefrehing()
    func didEndInfinitScroll(insertedIndx: [IndexPath])
    func removeCells(removeIndx: [IndexPath])
    func noticeNoMoreData()
}

class ProductListViewController: UIViewController, ProductListView, NavContainingLogo {

    var presenter: ProductListPresenter!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        addLogo()
        configTable()
        tableView.es.startPullToRefresh()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadVisibleCells()
        presenter.viewWillApear()
    }

    func addFavoritButton() {
        let buttonFav = UIBarButtonItem.init(image: UIImage.init(named: "favoriteNav.png"), style: .plain, target: self, action: #selector(self.tapOnFavorites(sender:)))
        buttonFav.imageInsets = UIEdgeInsets(top: 0, left: 4, bottom: -8, right: -4)
        self.navigationItem.rightBarButtonItems = [buttonFav]
    }

    @objc func tapOnFavorites(sender: UIBarButtonItem) {
        print(#function)
        presenter.openFavorites()
    }

}

extension ProductListViewController {

    func configTable() {
        self.tableView.register(ProductCell.self)
        self.tableView.rowHeight = 325

        tableView.es.addPullToRefresh {[weak self] in
            self?.refreshData()
        }

        tableView.es.addInfiniteScrolling {[weak self] in
            self?.presenter.getProducts()
        }
    }

    func refreshData() {
        self.presenter.pullToRefresh()
    }

    func reloadTable() {
        self.tableView.es.stopPullToRefresh()
        tableView.reloadData()
    }

    func didEndRefrehing() {
        self.tableView.es.stopPullToRefresh()
    }

    func didEndInfinitScroll(insertedIndx: [IndexPath]) {
        if insertedIndx.isEmpty {
            noticeNoMoreData()
            return
        }
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: insertedIndx, with: .fade)
            self.tableView.endUpdates()
            self.tableView.es.stopLoadingMore()
        }
    }

    func removeCells(removeIndx: [IndexPath]) {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: removeIndx, with: .automatic)
            self.tableView.endUpdates()
        }
    }

    func noticeNoMoreData() {
        self.tableView.es.noticeNoMoreData()
    }

}

extension ProductListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cellProduct = cell as? ProductCell  else { return }
        cellProduct.productImageView.kf.cancelDownloadTask()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProductCell
        let product = presenter.item(at: indexPath)
        cell.config(with: product)
        cell.favoriteClouser = { [weak self] in
            self?.presenter.tapOnFavorite(at: product.id)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectItem(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }

    func reloadVisibleCells() {
        if let rowsToReload = self.tableView.indexPathsForVisibleRows {
            self.tableView.reloadRows(at: rowsToReload, with: .none)
        }
    }
}
