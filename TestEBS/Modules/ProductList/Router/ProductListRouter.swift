//
//  ProductListRouter.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 25/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductListRouter {
    func openProductDetails(with productID: Int)
    func openProductDetails(with product: Product)

    func openFavorites()
}

class ProductListRouterImp: ProductListRouter {

    weak var viewController: ProductListViewController!

    func openProductDetails(with product: Product) {
        guard let detailsVC = ProductDetailsDefaultBuilder().buildProductDetailsViewController(with: product.id, product: product)
            else { print("Didn't init produc details module!"); return }

        guard let nav = viewController.navigationController
            else {
                viewController.present(detailsVC, animated: true, completion: nil)
                return
        }

        nav.pushViewController(detailsVC, animated: true)
    }

    func  openProductDetails(with productID: Int) {
        guard let detailsVC = ProductDetailsDefaultBuilder().buildProductDetailsViewController(with: productID)
            else { print("Didn't init produc details module!"); return }

        guard let nav = viewController.navigationController
            else {
                viewController.present(detailsVC, animated: true, completion: nil)
                return
        }

        nav.pushViewController(detailsVC, animated: true)
    }

    func openFavorites() {
        guard let vc = ProductListDefaultBuilder().buildFavoritesListVC()
            else { print("Didn't init favorites products module!"); return }

        guard let nav = viewController.navigationController
            else {
                viewController.present(vc, animated: true, completion: nil)
                return
        }
        nav.pushViewController(vc, animated: true)
    }

}
