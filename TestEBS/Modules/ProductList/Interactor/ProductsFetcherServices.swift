//
//  ProductsFetcherServices.swift
//  TestEBS
//
//  Created by Dinu_c on 2/25/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire

protocol ProductsFetcherServices: BaseServices {
    func getProducts(by offset: Int, completion: @escaping (Result<[Product]>) -> Void)
}

struct ProductsFetcherServicesImp: ProductsFetcherServices {
    var requestManager: SessionManager = Alam_DefaultHeaders.sender
    let limit: Int = constants.limitResults

    func getProducts(by offset: Int, completion: @escaping (Result<[Product]>) -> Void) {
        let params: [String: Any] = [NK.limit.rawValue: limit,
                                     NK.offset.rawValue: offset]
        request(RequestHttp(url: URLs.products, method: .get, parameters: params), completion: completion)
    }
}
