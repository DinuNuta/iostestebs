//
//  ProductListInteractor.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 25/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductListInteractor {
    func getProducts(by offset: Int, completion: @escaping (Result<[Product]>) -> Void)
    func liked(product: Product) throws
}

class ProductListInteractorImp: ProductListInteractor {
    var productFetcher: ProductsFetcherServices!
    var realmManager: RealmManager {RealmManagerImp.shared}

    init(productFetcher: ProductsFetcherServices) {
        self.productFetcher = productFetcher
    }

    func getProducts(by offset: Int, completion: @escaping (Result<[Product]>) -> Void) {
        self.productFetcher.getProducts(by: offset) {[weak self] (result) in
            do {
                let products = try result.get()
                guard !products.isEmpty else { completion(.success(products)); return }
                let ids = products.compactMap({$0.id})

                if let localId = self?.realmManager.getFavorites(by: ids).map({$0.id}),
                    !localId.isEmpty {
                    let filtered = products.filter({localId.contains($0.id)})
                    filtered.forEach({$0.isFavorite = true})
                    try self?.realmManager.add(entities: filtered)
                }
                completion(.success(products))
            } catch {
                completion(.failure(error))
            }
        }

    }

    func liked(product: Product) throws {
        try realmManager.liked(product: product)
    }
}
