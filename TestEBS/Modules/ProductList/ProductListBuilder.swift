//
//  ProductListBuilder.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 25/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit
import Swinject

protocol ProductListBuilder {
    func buildProductListViewController() -> ProductListViewController!
}

class ProductListDefaultBuilder: ProductListBuilder {

    let container = Container()

    func buildProductListViewController() -> ProductListViewController! {

        registerView()
        registerInteractor()
        registerRouter()
        registerPresenter()

        return container.resolve(ProductListViewController.self)!
    }

    func buildFavoritesListVC() -> ProductListViewController! {

        registerView()
        registerLocalFavoriteInteractor()
        registerRouter()
        registerFavoritesPresenter()

        return container.resolve(ProductListViewController.self)!
    }

    fileprivate func registerView() {
        container.register(ProductListViewController.self) { _ in

            ProductListViewController(nibName: String(describing: ProductListViewController.self), bundle: Bundle.main)

        }.initCompleted { rc, hc in

            hc.presenter = rc.resolve(ProductListPresenter.self)
        }
    }

    fileprivate func registerInteractor() {
        container.register(ProductListInteractor.self) { _ in
            ProductListInteractorImp(productFetcher: ProductsFetcherServicesImp())
        }
    }

    fileprivate func registerLocalFavoriteInteractor() {
        container.register(ProductListInteractor.self) { _ in
            FavoritesListInteractorImp()
        }
    }

    fileprivate func registerRouter() {
        container.register(ProductListRouter.self) { cont in
            let router = ProductListRouterImp()
            router.viewController = cont.resolve(ProductListViewController.self)!
            return router
        }
    }

    fileprivate func registerPresenter() {
        container.register(ProductListPresenter.self) { cont in
            ProductListPresenterImp(view: cont.resolve(ProductListViewController.self)!,
                                    interactor: cont.resolve(ProductListInteractor.self)!,
                                    router: cont.resolve(ProductListRouter.self)!)
        }
    }

    fileprivate func registerFavoritesPresenter() {
        container.register(ProductListPresenter.self) { cont in
            FavoritesListPresenterImp(view: cont.resolve(ProductListViewController.self)!,
                                      interactor: cont.resolve(ProductListInteractor.self)!,
                                      router: cont.resolve(ProductListRouter.self)!)
        }
    }

    deinit {
        container.removeAll()
    }
}
