//
//  Product.swift
//  TestEBS
//
//  Created by Dinu_c on 2/25/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@objcMembers class Product: Object, Codable {
    dynamic var id: Int = 0
    dynamic var title, shortDescription: String?
    dynamic var image: String?
    dynamic var price: Double = 0
    dynamic var salePrecent: Double = 0
    dynamic var details: String?
    dynamic var isFavorite: Bool = false

    override class func primaryKey() -> String? {
        return "id"
    }

    enum CodingKeys: String, CodingKey {
        case id, title
        case shortDescription
        case image, price
        case salePrecent
        case details
    }

    init(id: Int, title: String?, shortDescription: String?, image: String?, price: Double, salePrecent: Double, details: String?) {
        self.id = id
        self.title = title
        self.shortDescription = shortDescription
        self.image = image
        self.price = price
        self.salePrecent = salePrecent
        self.details = details
    }

    required init() {
        super.init()
    }
}
