//
//  ProductListPresenter.swift
//  TestEBS
//
//  Created by Coscodan Dinu on 25/02/2020.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

protocol ProductListPresenter {
    var numberOfItems: Int {get}

    func pullToRefresh()
    func getProducts()
    func item(at indexPath: IndexPath) -> Product
    func didSelectItem(at indexPath: IndexPath)
    func tapOnFavorite(at productID: Int)
    func openFavorites()
    func viewWillApear()
}

class ProductListPresenterImp: ProductListPresenter {

    weak var view: ProductListView!
    var interactor: ProductListInteractor!
    var router: ProductListRouter!

    var products: [Product] = []
    var numberOfItems: Int {
        return self.products.count
    }

    init(view: ProductListView, interactor: ProductListInteractor, router: ProductListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }

    internal func openFavorites() {
        router.openFavorites()
    }

    func pullToRefresh() {
        interactor.getProducts(by: 0) { [weak self] (result) in
            do {
                self?.products = try result.get()
                self?.view.reloadTable()
            } catch {
                self?.view.didEndRefrehing()
                self?.view.showAlertError(error: error)
            }
        }
    }

    func tapOnFavorite(at productID: Int) {
        do {
            guard let product = products.first(where: {$0.id == productID}) else { return }
            try interactor.liked(product: product)
        } catch {
            self.view.showAlertError(error: error)
        }
    }

    func viewWillApear() { }
}

extension ProductListPresenterImp {
    func getProducts() {
        interactor.getProducts(by: numberOfItems) { [weak self] (result) in
            guard let self = self else {return}
            do {
                let newItems = try result.get()
                let lastCount = self.products.count
                self.products.append(contentsOf: newItems)
                var indexPaths = [IndexPath]()
                for indx in lastCount ..< self.numberOfItems {
                    indexPaths.append(IndexPath(item: indx, section: 0))
                }
                self.view.didEndInfinitScroll(insertedIndx: indexPaths)
                if newItems.count < constants.limitResults {
                    self.view.noticeNoMoreData()
                }
            } catch {
                self.view.didEndInfinitScroll(insertedIndx: [])
                self.view.showAlertError(error: error)
            }
        }
    }

    func item(at indexPath: IndexPath) -> Product {
        return products[indexPath.row]
    }

    func didSelectItem(at indexPath: IndexPath) {
        let product = products[indexPath.row]
        router.openProductDetails(with: product)
    }

}
