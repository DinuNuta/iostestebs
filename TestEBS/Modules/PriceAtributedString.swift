//
//  PriceAtributedString.swift
//  TestEBS
//
//  Created by Dinu_c on 2/27/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import UIKit

protocol PriceAtributedString {
    func priceAtributedString(price: Double, salePrecent: Double,
                              priceFont: UIFont, priceColor: UIColor,
                              saleFont: UIFont, saleColor: UIColor) -> NSAttributedString
}

extension PriceAtributedString {
    func priceAtributedString(price: Double, salePrecent: Double,
                              priceFont: UIFont = UIFont.openSans_SemiBold,
                              priceColor: UIColor = .cobalt,
                              saleFont: UIFont = UIFont.openSans_SemiBold.withSize(18.0),
                              saleColor: UIColor = .nepal) -> NSAttributedString {
        var priceStr = "$ \(price)"
        var string = priceStr
        var saleStr = ""
        if 1..<100 ~= salePrecent {
            let oldPrice: Double = price/(100 - salePrecent) * 100
            saleStr = String(format: "$ %.2f", oldPrice)
            priceStr += ",- "
            string = priceStr + saleStr
        }

        let attributedString = NSMutableAttributedString(string: string)
        let priceFontAttribute = [NSAttributedString.Key.font: priceFont, NSAttributedString.Key.foregroundColor: priceColor]
        let saleFontAttribute = [NSAttributedString.Key.font: saleFont, NSAttributedString.Key.foregroundColor: saleColor]
        let saleStrikethroughAttribute = [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue]

        attributedString.addAttributes(priceFontAttribute, range: (string as NSString).range(of: priceStr))
        if !saleStr.isEmpty {
            attributedString.addAttributes(saleFontAttribute, range: (string as NSString).range(of: saleStr))
            attributedString.addAttributes(saleStrikethroughAttribute, range: (string as NSString).range(of: saleStr))
        }
        return attributedString
    }
}
