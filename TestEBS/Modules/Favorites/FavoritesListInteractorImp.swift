//
//  FavoritesListInteractorImp.swift
//  TestEBS
//
//  Created by Dinu_c on 2/28/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

class FavoritesListInteractorImp: ProductListInteractor {
    var realmManager: RealmManager {RealmManagerImp.shared}

    init() { }

    func getProducts(by offset: Int, completion: @escaping (Result<[Product]>) -> Void) {
        let localProducts = self.realmManager.getAllFavorites()
        completion(.success(localProducts))
    }

    func liked(product: Product) throws {
        try realmManager.liked(product: product)
    }
}
