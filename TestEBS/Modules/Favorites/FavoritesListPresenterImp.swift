//
//  FavoritesListPresenterImp.swift
//  TestEBS
//
//  Created by Dinu_c on 2/28/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
class FavoritesListPresenterImp: ProductListPresenterImp {

    override func pullToRefresh() {
        super.pullToRefresh()
        self.view.noticeNoMoreData()
    }

    override func tapOnFavorite(at productID: Int) {
        super.tapOnFavorite(at: productID)
        if let indx = products.firstIndex(where: {$0.id == productID}) {
            self.products.remove(at: indx)
            self.view.removeCells(removeIndx: [IndexPath(row: indx, section: 0)])
        }
    }

    override func viewWillApear() {
        let indexes = products.enumerated().filter({$0.element.isFavorite == false}).map({$0.offset})
        guard !indexes.isEmpty else { return }
        let indexPaths = indexes.compactMap({IndexPath(row: $0, section: 0)})
        DispatchQueue.main.async {
            self.products.removeAll(where: {!$0.isFavorite})
            self.view.removeCells(removeIndx: indexPaths)
        }

    }
}
