//
//  Constants.swift
//  TestEBS
//
//  Created by Dinu_c on 2/25/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
public let constants = Constants.shared

public struct Constants {

    static let shared = Constants()

    let networking_file = "file"
    /// Limit results returned
    let limitResults = 10

    private init() {}
}

typealias NK = NetworkKeys

enum NetworkKeys: String {
    case offset
    case limit
    case id
}
