//
//  RealmManager.swift
//  TestEBS
//
//  Created by Dinu_c on 2/27/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmManager {

    func get<Entity: Object>(entityType: Entity.Type) throws -> [Entity]
    func insert<Entity: Object>(entities: [Entity]) throws
    func add<Entity: Object>(entities: [Entity]) throws
    func add<Entity: Object>(entity: Entity) throws

    func remove<Entity: Object>(entities: [Entity]) throws
    func removeAll<Entity: Object>(entityType: Entity.Type) throws
    func removeAllDB() throws

    func get<Entity: Object>(entityType: Entity.Type, id: Int)throws -> Entity?
    func get<Entity: Object>(entityType: Entity.Type, ids: [Int])throws -> [Entity]

    func getFavorites(by ids: [Int]) -> [Product]
    func getAllFavorites() -> [Product]

    func reduceProducts() throws
    func liked(product: Product) throws
}

class RealmManagerImp: RealmManager {

    static let shared = RealmManagerImp()

    lazy var realm: Realm = try! Realm()

    func get<Entity>(entityType: Entity.Type) -> [Entity] where Entity: Object {
        return realm.objects(entityType).map {$0}
    }

    func insert<Entity>(entities: [Entity]) throws where Entity: Object {
        try realm.write {
            realm.add(entities, update: .error)
        }
    }

    func add<Entity>(entities: [Entity]) throws where Entity: Object {
        try realm.write {
            realm.add(entities, update: .modified)
        }
    }

    func add<Entity>(entity: Entity) throws where Entity: Object {
        try realm.write {
            realm.add(entity, update: .modified)
        }
    }

    func remove<Entity>(entities: [Entity]) throws where Entity: Object {
        try realm.write {
            realm.delete(entities)
        }
    }

    func removeAll<Entity>(entityType: Entity.Type) throws where Entity: Object {
        let allObjects = realm.objects(entityType)
        try realm.write {
            realm.delete(allObjects)
        }
    }

    func removeAllDB() throws {
        try realm.write {
            realm.deleteAll()
        }
    }

    func get<Entity>(entityType: Entity.Type, id: Int) -> Entity? where Entity: Object {
        return realm.objects(entityType).filter("id == %d", id).first
    }

    func get<Entity>(entityType: Entity.Type, ids: [Int]) -> [Entity] where Entity: Object {
        if !ids.isEmpty {
            return realm.objects(entityType).filter("id IN %d", ids).map {$0}
        } else {
            return []
        }
    }

}
extension RealmManagerImp {
    func liked(product: Product) throws {
        try realm.write {
            product.isFavorite = !product.isFavorite
            realm.add(product, update: .modified)
        }
    }

    func getFavorites(by ids: [Int]) -> [Product] {
        if !ids.isEmpty {
            return realm.objects(Product.self).filter("id IN %d AND isFavorite = true", ids).map({$0})
        } else {
            return []
        }
    }

    func getAllFavorites() -> [Product] {
        return realm.objects(Product.self).filter("isFavorite = true").map({$0})
    }

    func reduceProducts() throws {
        let allObjects = realm.objects(Product.self).filter("isFavorite = false")
        try realm.write {
            realm.delete(allObjects)
        }
    }
}

extension Realm {
    func objectsForPrimaryKeys<T: Object, K: AnyObject>(type: T.Type, keys: [K]) -> Results<T> {
        return objects(type).filter("\(type.primaryKey) IN %@", keys)
    }
}
