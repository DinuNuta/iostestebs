//
//  JSONModel.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

//public enum JSONParseError : Error {
//    case failedToParse(message:String)
//}

extension Decodable {
    init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw ErrorMessages.jsonParseError(message: "Data is nil")}
        let decoder = newJSONDecoder()

        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw ErrorMessages.jsonParseError(message: "Didn't found object for keypath : \(keyPath)") }
            self = try decoder.decode(Self.self, withJSONObject: nestedJson)
            return
        }
        self = try decoder.decode(Self.self, from: data)
    }
}
public extension Array where Element: Codable {
    init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw ErrorMessages.jsonParseError(message: "Data is nil" ) }
        let decoder = newJSONDecoder()

        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw ErrorMessages.jsonParseError(message: "Didn't found object for keypath : \(keyPath)")}
            self = try decoder.decode([Element].self, withJSONObject: nestedJson)
            return
        }
        self = try decoder.decode([Element].self, from: data)
    }
}

public protocol JSONModel: Codable {
    init(data: Data?, keyPath: String?) throws
}

public extension JSONModel {
    init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw ErrorMessages.jsonParseError(message: "Data is nil")}
        let decoder = newJSONDecoder()

        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw ErrorMessages.jsonParseError(message: "Didn't found object for keypath : \(keyPath)") }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try decoder.decode(Self.self, from: nestedData)
            return
        }
        self = try decoder.decode(Self.self, from: data)
    }
}

public extension Array where Element: JSONModel {
    init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw ErrorMessages.jsonParseError(message: "Data is nil" ) }
        let decoder = newJSONDecoder()

        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else { throw ErrorMessages.jsonParseError(message: "Didn't found object for keypath : \(keyPath)")}
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)

            self = try decoder.decode([Element].self, from: nestedData)
            return
        }
        self = try decoder.decode([Element].self, from: data)
    }
}

/// A decoder with:
/// dateDecodingStrategy = .iso8601
/// keyDecodingStrategy = .convertFromSnakeCase
///
/// - Returns: JSONDecoder
func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        //        decoder.dateDecodingStrategy = .iso8601
        decoder.dateDecodingStrategy = .secondsSince1970
    }
    return decoder
}

/// A encoder with:
/// dateEncodingStrategy = .iso8601
/// keyEncodingStrategy = .convertToSnakeCase
///
/// - Returns: JSONEncoder
func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    encoder.keyEncodingStrategy = .convertToSnakeCase
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

extension JSONEncoder {
    func encodeJSONObject<T: Encodable>(_ value: T, options opt: JSONSerialization.ReadingOptions = []) throws -> Any {
        let data = try encode(value)
        return try JSONSerialization.jsonObject(with: data, options: opt)
    }
}

extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, withJSONObject object: Any, options opt: JSONSerialization.WritingOptions = []) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: object, options: opt)
        return try decode(T.self, from: data)
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        do {
            return try newJSONEncoder().encodeJSONObject(self, options: .allowFragments) as? [String: Any]
        } catch {
            return nil
        }
    }
}

extension Decodable {
    init(from: Any) throws {
        let decoder = newJSONDecoder()
        self = try decoder.decode(Self.self, withJSONObject: from)
    }
}

struct JSONCodingKeys: CodingKey {
    var stringValue: String

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    var intValue: Int?

    init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

extension KeyedDecodingContainer {

    func decode(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> [String: Any] {
        let container = try self.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)
        return try container.decode(type)
    }

    func decodeIfPresent(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> [String: Any]? {
        guard contains(key) else {
            return nil
        }
        guard try decodeNil(forKey: key) == false else {
            return nil
        }
        return try decode(type, forKey: key)
    }

    func decode(_ type: Array<Any>.Type, forKey key: K) throws -> [Any] {
        var container = try self.nestedUnkeyedContainer(forKey: key)
        return try container.decode(type)
    }

    func decodeIfPresent(_ type: Array<Any>.Type, forKey key: K) throws -> [Any]? {
        guard contains(key) else {
            return nil
        }
        guard try decodeNil(forKey: key) == false else {
            return nil
        }
        return try decode(type, forKey: key)
    }

    func decode(_ type: Dictionary<String, Any>.Type) throws -> [String: Any] {
        var dictionary = [String: Any]()

        for key in allKeys {
            if let boolValue = try? decode(Bool.self, forKey: key) {
                dictionary[key.stringValue] = boolValue
            } else if let stringValue = try? decode(String.self, forKey: key) {
                dictionary[key.stringValue] = stringValue
            } else if let intValue = try? decode(Int.self, forKey: key) {
                dictionary[key.stringValue] = intValue
            } else if let doubleValue = try? decode(Double.self, forKey: key) {
                dictionary[key.stringValue] = doubleValue
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedDictionary
            } else if let nestedArray = try? decode(Array<Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedArray
            }
        }
        return dictionary
    }
}

extension UnkeyedDecodingContainer {

    mutating func decode(_ type: Array<Any>.Type) throws -> [Any] {
        var array: [Any] = []
        while isAtEnd == false {
            // See if the current value in the JSON array is `null` first and prevent infite recursion with nested arrays.
            if try decodeNil() {
                continue
            } else if let value = try? decode(Bool.self) {
                array.append(value)
            } else if let value = try? decode(Double.self) {
                array.append(value)
            } else if let value = try? decode(String.self) {
                array.append(value)
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self) {
                array.append(nestedDictionary)
            } else if let nestedArray = try? decode(Array<Any>.self) {
                array.append(nestedArray)
            }
        }
        return array
    }

    mutating func decode(_ type: Dictionary<String, Any>.Type) throws -> [String: Any] {

        let nestedContainer = try self.nestedContainer(keyedBy: JSONCodingKeys.self)
        return try nestedContainer.decode(type)
    }
}

struct DynamicKey: CodingKey {

    var stringValue: String

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    var intValue: Int? { return nil }

    init?(intValue: Int) { return nil }

}

extension KeyedEncodingContainer where Key == DynamicKey {

    mutating func encodeDynamicKeyValues(withDictionary dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let dynamicKey = DynamicKey(stringValue: key)!
            // Following won't work:
            // let v = value as Encodable
            // try propertiesContainer.encode(v, forKey: dynamicKey)
            // Therefore require explicitly casting to the supported value type:
            switch value {
            case let val as String: try encode(val, forKey: dynamicKey)
            case let val as Int: try encode(val, forKey: dynamicKey)
            case let val as Double: try encode(val, forKey: dynamicKey)
            case let val as Float: try encode(val, forKey: dynamicKey)
            case let val as Bool: try encode(val, forKey: dynamicKey)
            default: break
            }
        }
    }

}

extension KeyedDecodingContainer where Key == DynamicKey {

    func decodeDynamicKeyValues() -> [String: Any] {
        var dict = [String: Any]()
        for key in allKeys {
            // Once again, following decode doesn't work, therefore requires explicitly decoding each supported type.
            // propertiesContainer.decode(?, forKey: key)
            if let val = try? decode(String.self, forKey: key) {
                dict[key.stringValue] = val
            } else if let val = try? decode(Bool.self, forKey: key) {
                dict[key.stringValue] = val
            } else if let val = try? decode(Int.self, forKey: key) {
                dict[key.stringValue] = val
            } else if let val = try? decode(Double.self, forKey: key) {
                dict[key.stringValue] = val
            } else if let val = try? decode(Float.self, forKey: key) {
                dict[key.stringValue] = val
            } else {

            }
        }
        return dict
    }

}
