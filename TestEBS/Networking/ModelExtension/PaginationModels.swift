//
//  PaginationModels.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

struct Pagination<T: Codable>: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [T]
}

struct PaginationDict<T: Codable>: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: T
}
