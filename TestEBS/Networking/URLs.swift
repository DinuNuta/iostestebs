//
//  URLs.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

public struct URLs {
    static var base = URL(string: "http://mobile-test.devebs.net:5000")!
    static var api = base
    static var products = api.appendingPathComponent("products")
    static var product = api.appendingPathComponent("product")

}

public extension URL {
    func appendingQuery(withParameters parameters: [URLQueryItem]) -> URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters }
        return components.url!
    }

    var queryItems: [URLQueryItem] {
        let components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        return components.queryItems ?? []
    }

}
