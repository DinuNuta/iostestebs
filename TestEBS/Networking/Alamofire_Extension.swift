//
//  Alamofire_Extention.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias Result<T> = Alamofire.Result<T>

extension Result {
    public func get() throws -> Value {
        return try self.unwrap()
    }
}

class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String

    init(accessToken: String) {
        self.accessToken = accessToken
    }

    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue(accessToken, forHTTPHeaderField: "AUTHENTICATION")
        return urlRequest
    }
}

public extension Notification.Name {
    static let tokenInviled = Notification.Name("TokenInviled")
    static let logOut = Notification.Name("LogOut")
}

extension NetworkReachabilityManager {
    var networkReachabilityStatusDescription: String {
        switch self.networkReachabilityStatus {
        case .notReachable:
            return "Not Reachable"
        case .reachable(let connectionType):
            switch connectionType {
            case .ethernetOrWiFi:
                return "Connected through ethernet or WiFi"
            case .wwan:
                return "Connected through wwan"
            }
        case .unknown:
            return "unknown"
        }
    }
}

extension DataRequest {

    public var errorInterpreter: CDErrorInterpreting {
        return ErrorInterpreter()
    }

    public func validateErrors() -> Self {

        //        return validate(contentType: ["application/json"])
        return validate { request, response, data in
            //   FOR DEBUG ---------------------------------
            print(response.debugDescription)
            print(request.debugDescription)
            if let bData = data {
                print(try? JSON(data: bData))
            }
            // ---------------------------------------------
            if let networkErorr = self.errorInterpreter.error(with: response, data: data) {
                if response.statusCode == 401 {
                    print("Token expired. Please add implementation for this case")
                    //add implementation for token expired status code
                }
                return.failure(networkErorr)
            } else {
                if response.statusCode >= 400 {
                    let reason: AFError.ResponseValidationFailureReason = .unacceptableStatusCode(code: response.statusCode)
                    return .failure(AFError.responseValidationFailed(reason: reason))
                }
            }
            return.success
        }.validate(statusCode: 200..<300)
    }

    func formatReasonString(with json: JSON) -> String? {
        guard let message = json.dictionary
            else { return nil }

        return message.reduce("") { (messageString, element : (key: String, value: JSON)) -> String in
            let errkey = humanReadableKey(string: element.key)
            let string = element.value.array?.reduce("") { (errorReasonString, element) -> String in
                if let reason = element.string.map({" - " + $0 + "\n"}) {
                    return errorReasonString + reason
                } else {
                    return errorReasonString
                }
                } ?? " " + element.value.stringValue
            return messageString + errkey + string
        }
    }

    public func humanReadableKey(string: String) -> String {
        return string.capitalized.replacingOccurrences(of: "_", with: " ")
    }

}

// MARK: - Alamofire response handlers
public extension DataRequest {
    func decodableArrayResponseSerializer<T: Decodable>(keyPath: String? = nil) -> DataResponseSerializer<[T]> {
        return DataResponseSerializer { _, _, data, error in
            guard error == nil else { return .failure(error!) }
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            return Result { try [T](data: data, keyPath: keyPath)}
        }
    }

    func decodableResponseSerializer<T: Decodable>(keyPath: String? = nil) -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, _, data, error in
            guard error == nil else { return .failure(error!) }
            guard let data = data, !data.isEmpty else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNilOrZeroLength))
            }
            return Result { try T(data: data, keyPath: keyPath)}
        }
    }

    func decodableOptionalResponseSerializer<T: Decodable>(keyPath: String? = nil) -> DataResponseSerializer<T?> {
        return DataResponseSerializer { _, _, data, error in
            guard error == nil else { return .failure(error!) }
            guard let data = data, !data.isEmpty else {
                return .success(nil)
            }
            return Result { try T(data: data, keyPath: keyPath)}
        }
    }

    @discardableResult
    func responseDecodable<T: Codable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableArrayResponseSerializer(keyPath: keyPath), completionHandler: completionHandler)
    }

    @discardableResult
    func responseDecodable<T: Codable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(keyPath: keyPath), completionHandler: completionHandler)
    }

    @discardableResult
    func responseDecodable<T: Codable>(queue: DispatchQueue? = nil, keyPath: String? = nil, completionHandler: @escaping (DataResponse<T?>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableOptionalResponseSerializer(keyPath: keyPath), completionHandler: completionHandler)
    }

    //    @discardableResult
    //    func responseUsers(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<[User]>) -> Void) -> Self {
    //        return responseDecodable(queue: queue, completionHandler: completionHandler)
    //    }
    //    @discardableResult
    //    func responseUser(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<User>) -> Void) -> Self {
    //        return responseDecodable(queue: queue, completionHandler: completionHandler)
    //    }
    //    @discardableResult
    //    func responseLeaderBoard(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PaginationDict<LeaderBoard>>) -> Void) -> Self {
    //        return responseDecodable(queue: queue, completionHandler: completionHandler)
    //    }
    //    @discardableResult
    //    func responseVault(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Pagination<[Vault]>>) -> Void) -> Self {
    //        return responseDecodable(queue: queue, completionHandler: completionHandler)
    //    }

}

//extension DataRequest {
//    //add for each Codablemodel
//    @discardableResult
//    func responseMessage(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<Message>) -> Void) -> Self {
//        return responseDecodable(queue: queue, completionHandler: completionHandler)
//    }
//
//    @discardableResult
//    func responseKYC(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<KYCData>) -> Void) -> Self {
//        return responseDecodable(queue: queue, completionHandler: completionHandler)
//    }
//
//    @discardableResult
//    func responseBanks(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<[BankInfo]>) -> Void) -> Self {
//        return responseDecodable(queue: queue, completionHandler: completionHandler)
//    }
//
//    @discardableResult
//    func responseBankDetails(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<BankDetail>) -> Void) -> Self {
//        return responseDecodable(queue: queue, completionHandler: completionHandler)
//    }
//
//    @discardableResult
//    func responsePayoutMethods(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PayoutMethods?>) -> Void) -> Self {
//        return responseDecodable(queue: queue, completionHandler: completionHandler)
//    }
//}
