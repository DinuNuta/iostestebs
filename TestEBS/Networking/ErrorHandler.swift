//
//  ErrorHandler.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

/// Status Code of request error
///
/// - OK:                   200
/// - Created:              201
/// - NoContent:            204
/// - NotModified:          304
/// - BadRequest:           400
/// - Unauthorized:         401
/// - Forbidden:            403
/// - NotFound:             404
/// - UnprocessableEntity:  422
/// - ToManyRequests:       429
/// - InteralServerError:   500
public enum StatusCode: Int {
    case ok                  = 200
    case created             = 201
    case noContent           = 204
    case notModified         = 304

    case badRequest          = 400
    case unauthorized        = 401
    case forbidden           = 403
    case notFound            = 404
    case methodNotAllowed    = 405
    case unprocessableEntity = 422
    case locked              = 423
    case updateRequired      = 426
    case toManyRequests      = 429
    case interalServerError  = 500
}

/// Validation ErrorCodes
///
/// - AccountNotConfirmed: 4221
/// - InvalidOldPassword//: 4222
/// - InvalidEmailOrPassword//: 4223
/// - PasswordResetCodeExpired//: 4224
public enum ValidationErrorCodes: Int {
    case accountNotConfirmed = 4221
    case invalidOldPassword //= 4222
    case invalidEmailOrPassword// = 4223
    case passwordResetCodeExpired = 4224
    case baseValidationError = 422
}

public enum AuthenticationErrorCodes: Int {
    case invalidToken = 4011
    case tokenExpired //4012
    case invalidAppToken //4013
    case invalidSessionToken = 4014
    case accountNotConfirmed = 4221
    case invalidEmailOrPassword = 4223
}

/// Error related of sending Network Requests
public enum NetworkError: Error, CustomStringConvertible {
    case defaultError
    case unprocessableEntity(error: ValidationError, response: ErrorResponse?)
    case unauthorized(error: AuthenticationError)
    case badRequest
    case forbidden
    case notFound
    case methodNotAllowed
    case notModified
    case updateRequired
    case toManyRequests
    case interalServerError

    public var description: String {
        switch self {
        case .defaultError:
            return "Undexpected error"
        case .badRequest :
            return "Debug: Bad request check api"
        case .forbidden :
            return "Forbidden"
        case .interalServerError :
            return "Server is not avalible"
        case .notFound :
            return "Not found address"
        case .methodNotAllowed :
            return "Method Not Allowed"
        case .notModified :
            return "Not Modified"
        case .toManyRequests :
            return "Debug: To many requests"
        case .updateRequired:
            return "error_update_app"
        case .unauthorized(error: let error):
            return error.description
        case .unprocessableEntity(error: let error, response: let details):
            return error.description + (details?.localizedDescripiton ?? "")
        }
    }

    public var localizedDescription: String {
        return description.localizedCapitalized
    }

    public init?(with statusCode: StatusCode, errorResponse: ErrorResponse?) {
        switch statusCode {
        case .badRequest:
            self = .badRequest
        case .forbidden:
            self = .forbidden
        case .notFound:
            self = .notFound
        case .methodNotAllowed:
            self = .methodNotAllowed
        case .notModified:
            self = .notModified
        case .toManyRequests:
            self = .toManyRequests
        case .interalServerError:
            self = .interalServerError
        case .updateRequired:
            self = .updateRequired
        case .unprocessableEntity, .locked:
            if let error = errorResponse?.code.flatMap({ValidationErrorCodes(rawValue: $0)})
                .flatMap({ValidationError(with: $0)}) {         //some stuff to determine type of validation error on client in future, consider deleting It.
                self = .unprocessableEntity(error: error, response: errorResponse)
            } else {
                self = .unprocessableEntity(error: NetworkError.ValidationError.baseValidationError, response: errorResponse)
            }
        case .unauthorized:
            if let error = errorResponse?.code.flatMap({AuthenticationError.init(rawValue: $0)}) {
                self = .unauthorized(error: error)
            } else {
                self = .unauthorized(error: NetworkError.AuthenticationError.accountNotConfirmed)
            }
        default:
            return nil
        }
    }

    public enum ValidationError: CustomStringConvertible, Error {
        case accountNotConfirmed
        case invalidOldPassword
        case invalidEmailOrPassword
        case passwordResetCodeExpired
        case baseValidationError

        //some stuff to determine type of validation erro on client in future, consider deleting It.
        public init?(with code: ValidationErrorCodes) {
            switch code {
            case .baseValidationError:
                self = .baseValidationError
            case .accountNotConfirmed:
                self = .accountNotConfirmed
            case .invalidEmailOrPassword:
                self = .invalidEmailOrPassword
            case .invalidOldPassword:
                self = .invalidOldPassword
            case .passwordResetCodeExpired:
                self = .passwordResetCodeExpired
            }
        }

        public var description: String {
            switch self {
            case .accountNotConfirmed:
                return "Account is not confirmed."
            case .passwordResetCodeExpired:
                return "Reset code has beed expired"
            case .invalidEmailOrPassword:
                return "Invalid email or password"
            case .baseValidationError:
                return "Validation error"
            case .invalidOldPassword:
                return "Invalid old password"
            }
        }
        public var localizedDescription: String {
            return description
        }
    }

    public enum AuthenticationError: Int, CustomStringConvertible, Error {
        case accountNotConfirmed = 4011
        case invalidToken = 4031
        case tokenExpired = 4032
        case invalidAppToken = 4013
        case invalidSessionToken = 4014

        case invalidEmailOrPassword = 4223
        public var description: String {
            switch self {
            case .invalidToken:
                return "Debug: invalid Authentication token"
            case .tokenExpired:
                return "Base token expired"
            case .invalidAppToken:
                return "Debug: invalid app token"
            case .invalidSessionToken:
                return "Debug: invalid session token"
            case .accountNotConfirmed:
                return "Account is not confirmed"
            case .invalidEmailOrPassword:
                return "Invalid email or password"
            }
        }

        public var localizedDescription: String {
            return description
        }
    }
}
