//
//  ErrorMessages.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire
/// Local Errors
/// - JsonParseError: Parsing errors or Mapping models errors
/// - ToJSONConvertError: Errors in converting entity to JSON
/// - AFError: Alamofire errors with message
/// - NetworkError: A network error
public enum ErrorMessages: Error, CustomStringConvertible {
    case jsonParseError(message: String)
    case toJSONConvertError(message: String)
    case afError(error: AFError, title: String, message: String)
    case networkError(error: NetworkError, title: String, message: String)
    case noInternetConnection(message: String?)

    //    case CoreDataFetchError(message:String)
    //    case CoreDataSaveContextError(message:String)
    public var description: String {
        switch  self {
        case .jsonParseError(message: let description):
            return "JsonParseError : \(description)"
        case .toJSONConvertError(message: let description):
            return "Convert To JSON Error : \(description)"
        case .afError(let error, let title, let message):
            return "\(title) \n \(error.errorDescription ?? "") \n \(message)"
        case .networkError(let error, let title, let message):
            return "\(title) \n \(error.description) \n \(message)"
        case .noInternetConnection(message: let description):
            return "No internet connection" + (description.flatMap({": \($0)"}) ?? "")
        }
    }

    public var debugDescription: (title: String, message: String) {
        switch  self {
        case .afError(let error, let title, let message):
            return (title, "\(error.errorDescription ?? "") \n \(message)")
        case .networkError(let error, let title, let message):
            return (title, "\(error.description) \n \(message)")
        default:
            return ("", self.description)
        }
    }

    public var localizedDescription: String {
        return description
    }
}
