//
//  SessionManager.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import Alamofire

struct Alam_DefaultHeaders {
    public static let sender: SessionManager = {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        defaultHeaders["Accept"] = "application/json"
        //defaultHeaders["LANGUAGE"] = Locale.current.languageCode ?? "en"

        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = defaultHeaders

        return Alamofire.SessionManager(configuration: configuration)
    }()
}

struct Authorised {
    //    static var token : String { return PersistentDataManager.getSessionToken() ?? ""}

    //FIXEME: refactoring TOKEN struct
    static var token: String { return "Token_m9L23MyrBgtcj_pBzasYV96CTqNNmT28JXjtbrmrMPo"}
    static var sender: SessionManager {
        Alam_DefaultHeaders.sender.adapter = AccessTokenAdapter(accessToken: token) as RequestAdapter
        return Alam_DefaultHeaders.sender
    }
    static func remove() {
        Alam_DefaultHeaders.sender.adapter = AccessTokenAdapter(accessToken: "AUTHENTICATION") as RequestAdapter
        Alam_DefaultHeaders.sender.adapter = nil
    }
}
