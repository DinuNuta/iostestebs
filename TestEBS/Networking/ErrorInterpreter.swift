//
//  ErrorInterpreter.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

public struct ErrorResponse: Codable {
    var code: Int?
    var message: String?
    var error: String?
    let title: String?
    var errors: [ValidationErrorResponse]?

    var localizedDescripiton: String {
        if let validationError = errors?.reduce("", { (_, validationError) -> String? in
            return validationError.localizedDescripiton.flatMap({ $0 + "\n"})
        }) {
            return  (message?.localizedLowercase ?? "") + validationError
        }

        if let errorDescription = error?.localizedLowercase {
            return errorDescription
        }

        if let description = message?.localizedLowercase {
            return description
        }
        return ""
    }
}

public struct ValidationErrorResponse: Codable {
    var message: String?
    var field: String?

    var localizedDescripiton: String? {
        return message?.localizedLowercase
    }

    var debugDescription: String? {
        return (field.flatMap({"KeyField: \($0)"}) ?? "") + (message.flatMap({" \($0.localizedLowercase) + \n"}) ?? "")
    }
}

public protocol CDErrorInterpreting {
    func error(with response: HTTPURLResponse?, data: Data?) -> Error?
}
public struct ErrorInterpreter: CDErrorInterpreting {
    public init() {}

    public func error(with response: HTTPURLResponse?, data: Data?) -> Error? {
        guard let statusCode = response.flatMap({StatusCode.init(rawValue: $0.statusCode)})
            else { return nil }

        let error = try? ErrorResponse(data: data, keyPath: nil)
        guard let networkError = NetworkError.init(with: statusCode, errorResponse: error) else { return nil }
        return ErrorMessages.networkError(error: networkError, title: error?.title ?? "",
                                          message: error?.localizedDescripiton ?? networkError.localizedDescription)
    }
}
