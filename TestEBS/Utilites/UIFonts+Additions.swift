//
//  UIFonts+Additions.swift
//  TestEBS
//
//  Created by Dinu_c on 2/26/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class var openSans_SemiBold: UIFont {
        return UIFont(name: "OpenSans-Semibold", size: 27.0)!
    }

    class var openSans_Bold: UIFont {
        return UIFont(name: "OpenSans-Bold", size: 16.0)!
    }
}
