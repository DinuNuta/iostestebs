//
//  UIColor+Additions.swift
//  TestEBS
//
//  Created by Dinu_c on 2/11/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import UIKit

extension UIColor {
    /// base Bold text color
    @nonobjc class var cerulean: UIColor {
        return UIColor(named: "Cerulean") ?? UIColor(red: 7.0 / 255.0, green: 25.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
    }
    ///prices color
    @nonobjc class var cobalt: UIColor {
        return UIColor(named: "Cobalt")!
    }
    ///old price
    @nonobjc class var nepal: UIColor {
        return UIColor(named: "Nepal")!
    }
    ///light text color
    @nonobjc class var rhino: UIColor {
        return UIColor(named: "Rhino")!
    }
    ///nav bar color
    @nonobjc class var midnightBlue: UIColor {
        return UIColor(named: "MidnightBlue")!
    }

    @nonobjc class var slatePurple: UIColor {
        return UIColor(named: "SlatePurple")!
    }

}
