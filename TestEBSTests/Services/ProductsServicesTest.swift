//
//  ProductsServicesTest.swift
//  TestEBSTests
//
//  Created by Dinu_c on 2/25/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import XCTest
@testable import TestEBS
@testable import Alamofire

class ProductsServicesTest: XCTestCase {

    var sut: ProductsFetcherServicesImp!

    override func setUp() {
        super.setUp()

        let manager: SessionManager = {
            let configuration: URLSessionConfiguration = {
                let configuration = URLSessionConfiguration.default
                configuration.protocolClasses = [MockURLProtocol.self]
                return configuration
            }()

            return SessionManager(configuration: configuration)
        }()
        sut = ProductsFetcherServicesImp(requestManager: manager)
    }

    override func tearDown() {
        sut = nil
    }

    private var dataProductList: Data {
        let testBundle = Bundle(for: ProductTest.self)
        let path = testBundle.path(forResource: "FakeProductsData", ofType: "json")!

        do {
            return try Data.init(contentsOf: URL.init(fileURLWithPath: path))
        } catch {
            XCTFail(error.localizedDescription)
            return Data()
        }
    }

    func testReturnLaunches() {
        MockURLProtocol.response(url: URLs.products, data: dataProductList, status: 200)

        let expectation = XCTestExpectation(description: "Performs a request")

        sut.getProducts(by: 0) { (result) in
            do {
                let products = try result.get()
                XCTAssertFalse(products.isEmpty)
                expectation.fulfill()
            } catch {
                XCTFail(error.localizedDescription)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 3)
    }

    //    func testPerformanceExample() {
    //        // This is an example of a performance test case.
    //        self.measure {
    //            // Put the code you want to measure the time of here.
    //        }
    //    }

}
