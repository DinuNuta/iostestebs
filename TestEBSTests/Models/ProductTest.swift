//
//  ProductTest.swift
//  TestEBSTests
//
//  Created by Dinu_c on 2/25/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import XCTest
@testable import TestEBS

class ProductTest: XCTestCase {

    private var dataSinglproduct: Data {
        let testBundle = Bundle(for: ProductTest.self)
        let path = testBundle.path(forResource: "FakeProductSingleData", ofType: "json")!

        do {
            return try Data.init(contentsOf: URL.init(fileURLWithPath: path))
        } catch {
            XCTFail(error.localizedDescription)
            return Data()
        }
    }

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func assertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String, decoding: T.Type, from data: Data, file: StaticString = #file, line: UInt = #line) {
        XCTAssertThrowsError(try newJSONDecoder().decode(decoding, from: data), file: file, line: line) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.", file: file, line: line)
            } else {
                XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)", file: file, line: line)
            }
        }
    }

    func testDecoding_whenMissingId_itThrows() throws {
        let data_ = try dataSinglproduct.json(deletingKeyPaths: "id")
        assertThrowsKeyNotFound("id", decoding: Product.self, from: data_)
    }

    func testDecoding_whenMissingTitle_itThrows() throws {
        let data_ = try dataSinglproduct.json(deletingKeyPaths: "title")
        assertThrowsKeyNotFound("title", decoding: Product.self, from: data_)
    }

    func testItShouldThrowAnErrorIfDataIsNil() {
        XCTAssertThrowsError(try Product(data: nil), "shold throw error with description that data is nil") { (error) in
            print(error)
        }
    }

    func testShouldCreatedCorrectFieldsFromJSON() {
        do {
            let product = try Product(data: dataSinglproduct)

            XCTAssertEqual(product.id, 12)
            XCTAssertEqual(product.title, "Mini frigider")
            XCTAssertEqual(product.shortDescription, "Mini frigider pentru birou USB")
            let expegtedDetail = "Cadouri super cool!  Vine vremea calda si odata cu ea si nevoile noastre sufera modificari." +
                " Din seria de gadgeturi funny pentru birou, iti prezentam super Mini frigiderul USB. " +
                "Este un gadget ce iti raceste doza sau sticla de suc cat timp tu iti vezi de alte treburi, cu ajutorul alimentarii prin USB direct de la laptop." +
            " Vinerea, noi avem casual friday si la final de program savuram cate o bere, asa ca deja ne-am facut cadou frigiderul :)."
            //            XCTAssertEqual(product.details, expegtedDetail)
            XCTAssertEqual(product.salePrecent, 28)
            XCTAssertEqual(product.price, 100)
            XCTAssertEqual(product.image, "https://s11emagst.akamaized.net/products/5365/5364596/images/res_313150d518be7a19ee4f88c609e62930_450x450_1ete.jpg")

        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testItShouldThrowAnErrorIfDataIsInvalid() {
        let data = "{\"invalidData\" : 0}".data(using: .utf8)
        XCTAssertThrowsError(try Product(data: data), "shold throw a decoding error") { (error) in
            print(error)
        }
    }

    //    func testPerformanceExample() {
    //        // This is an example of a performance test case.
    //        self.measure {
    //            // Put the code you want to measure the time of here.
    //        }
    //    }

}

extension Data {
    func json(updatingKeyPaths keyPaths: (String, Any)...) throws -> Data {
        let decoded = try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as AnyObject

        for (keyPath, value) in keyPaths {
            decoded.setValue(value, forKeyPath: keyPath)
        }

        return try JSONSerialization.data(withJSONObject: decoded)
    }

    func json(deletingKeyPaths keyPaths: String...) throws -> Data {
        let decoded = try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as AnyObject

        for keyPath in keyPaths {
            decoded.setValue(nil, forKeyPath: keyPath)
        }

        return try JSONSerialization.data(withJSONObject: decoded)
    }
}
