# TestEBS

### TestEBS Test App technical tasks:
- [Sources Link](https://drive.google.com/drive/folders/1X5Jc8UV58xta_EB22HD-wYxUzeQz9E5i?usp=sharing)
- [API Link](http://mobile-test.devebs.net:5000/products)

Product list
- Create View for display list of products 
    ⁃    create custom cell like in design 
    ⁃    add table view with pagination 
    ⁃    add pagination logic 
- GET Products:  Create services for get list (create interactor, services, data model).
    offset and limit  = 10

Details 
- GET Details of product: Create services for getting details info. 
- Create view for displaying Details of selected product.

Favorits
- Implement Realm local data base
- create services to store and get list of favorites product.
- create interactor to work with local data base.
- add functionality to init Products module for Favorites.


### Installation

Requires [Cocoapods](https://cocoapods.org/)  to run.

First of all clone the repository and install pods.

```sh
$ pod install
```

Then Clean the Build folder and then Build and Run.

### Generamba

To create new module all you need is run in the terminal - `generamba gen NameOfYourModule swifty_viper`
And all classes and dependencies will be created for you in `Modules/` folder


### Swiftlint

* [Swiftlint](https://github.com/realm/SwiftLint)
```brew install swiftlint```
To autocorrect the code, run:
```swiftlint autocorrect --format```
To get all rules, run:
```swiftlint rules```
